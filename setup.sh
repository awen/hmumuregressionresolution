export HmumuRegressionResolutionDir=$PWD
source venv_HmumuRegressionResolution/bin/activate
#export X509_USER_PROXY=${HmumuRegressionResolutionDir}/grid_proxy
export MassRegressionResolutionDir=${HmumuRegressionResolutionDir}/massresolutionregression/
source ${MassRegressionResolutionDir}/setup/setup_env.sh
source RooFitExtensions/build/setup.sh
export PYTHONPATH=${HmumuRegressionResolutionDir}/utils_regression:$PYTHONPATH
#chmod 777 ${X509_USER_PROXY}
