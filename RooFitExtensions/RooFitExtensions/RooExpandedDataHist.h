// vim: ts=4:sw=4
/**********************************************************************************
 * Project: HistFitter - A ROOT-based package for statistical data analysis       *
 * Package: HistFitter                                                            *
 * Class  : RooExpandedDataHist                                                  *
 *                                                                                *
 * Description:                                                                   *
 *      Class derived from RooFitResult, to be able to add more parameters        *
 *      for error propagation (calculation & visualization)                       *
 *                                                                                *
 * Authors:                                                                       *
 *      HistFitter group, CERN, Geneva                                            *
 *                                                                                *
 * Redistribution and use in source and binary forms, with or without             *
 * modification, are permitted according to the terms listed in the file          *
 * LICENSE.                                                                       *
 **********************************************************************************/

#ifndef ROOEXPANDEDDATAHIST_H
#define ROOEXPANDEDDATAHIST_H

#include "TString.h"
#include "RooDataHist.h"
#include "RooAbsArg.h"
#include "RooArgList.h"
#include "RooListProxy.h"
#include "RooAbsListContainer.h"

#include <iostream>
#include <vector>

class RooExpandedDataHist: public RooDataHist {

 public:
  RooExpandedDataHist() {} ;
  RooExpandedDataHist(RooDataHist& dh, const char *name=0);
  RooExpandedDataHist(const RooExpandedDataHist& other, const char* name=0) ;

  virtual TObject* Clone(const char* newname=0) const { return new RooExpandedDataHist(*this,newname?newname:GetName()) ; }

  virtual ~RooExpandedDataHist();

  RooArgList& paramList() { return _container.paramList(); }
  const RooArgList& paramList() const { return _container.paramList(); }

  void updateParamList(const RooArgList& newParams) {

/*
    if (_container==0) { // || _firstCall) {
      //if (_container!=NULL) { delete _container; _container=0; }
      _container = new RooAbsListContainer(TString(GetName())+"containerName", TString(GetName())+"containerTitle", *dynamic_cast<RooDataHist*>(this) ) ;

      _firstCall = false;
    } 
*/

    //paramList().assignValueOnly(newParams) ;
    paramList().assignFast(newParams) ;

    _cache_sum_valid = kFALSE ;
  }

  void applyScaleFactor(Bool_t value) { _applyScaleFactor=value; }
  Double_t scaleFactor() { Int_t ibin = calcTreeIndex(); return _container.parVal(ibin); }

 protected:

  virtual Double_t get_wgt(const Int_t& idx)   const; 
  virtual Double_t get_curWeight()   const; 

  RooAbsListContainer _container; 

  Bool_t _firstCall;

  Bool_t   _applyScaleFactor;

 private:
  ClassDef(RooExpandedDataHist,2) // Container class for expanded fit result
};

#endif
