// vim: ts=4:sw=4 
/**********************************************************************************
 * Project: HistFitter - A ROOT-based package for statistical data analysis       *
 * Package: HistFitter                                                            *
 * Class  : RooExpandedDataHist                                                  *
 * Created: March 2012
 *                                                                                *
 * Description:                                                                   *
 *      Implementation (see header for description)                               *
 *                                                                                *
 * See corresponding .h file for author and license information                   *         
 *                                                                                *
 **********************************************************************************/

#include "RooFitExtensions/RooExpandedDataHist.h"

#include "RooRealVar.h"
#include <iostream>
using namespace std;

ClassImp(RooExpandedDataHist);

//_______________________________________________________________________________________
RooExpandedDataHist::RooExpandedDataHist(RooDataHist& dh, const char* name) : 
  RooDataHist(dh,name),
  _container( TString(name)+"containerName", TString(name)+"containerTitle", dh ),
  _firstCall(true),
  _applyScaleFactor(kTRUE)
{
}


//_____________________________________________________________________________
RooExpandedDataHist::RooExpandedDataHist(const RooExpandedDataHist& other, const char* name) :  
  RooDataHist(other,name), 
  _container(other._container),
  _firstCall(true),
  _applyScaleFactor(other._applyScaleFactor)
{ 
} 


//_____________________________________________________________________________
RooExpandedDataHist::~RooExpandedDataHist()
{
  //if (_container!=0) { delete _container; }
}


//_____________________________________________________________________________
Double_t 
RooExpandedDataHist::get_wgt(const Int_t& idx)   const 
{
  Double_t val= RooDataHist::_wgt[idx];

  if (_applyScaleFactor) 
    val *= _container.parVal(idx);
  return val;
}


//_____________________________________________________________________________
Double_t 
RooExpandedDataHist::get_curWeight()   const 
{
  Int_t idx = RooDataHist::_curIndex;

  Double_t val=RooDataHist::_curWeight;

  if (_applyScaleFactor) 
    val *= _container.parVal(idx);
  return val;
}
