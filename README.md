## Common software used in data analysis:
uproot: https://github.com/scikit-hep/uproot
root_numpy: http://scikit-hep.org/root_numpy/
ROOT: https://root.cern.ch/
RooFit: https://root.cern.ch/roofit-20-minutes, https://root.cern.ch/download/doc/RooFit_Users_Manual_2.91-33.pdf
Documentation on how to use computecanada: https://docs.computecanada.ca/wiki/Running_jobs

CERN releases software packages called LCG views. These are great ways to setup an envorinment with popular python software such as: uproot, matplotlib, jupyter, ROOT, tensorflow, keras etc. Documentation of available software is shown here: https://ep-dep-sft.web.cern.ch/document/lcg-releases .

When working with python it is common to work with virtual environments, information about those is found at this link: https://docs.python.org/3/tutorial/venv.html .

## Connecting to compute canada
```
ssh USER@graham.computecanada.ca
```

## Setup
We tend to work inside of these things called "singularity" containers. These emulate operating systems, such as centos7 or slc6. Currently ATLAS is transitioning from slc6 to centos7, and compute canada requires you to work inside of a container when using ATLAS software. In order to setup ATLAS software, you need to copy the following lines into your .bashrc file.

```
if [ -d /project/atlas/Tier3/ ]; then
        source /project/atlas/Tier3/AtlasUserSiteSetup.sh
        alias sa='setupATLAS -c slc6+batch'
fi
```

The execute your .bashrc file:
```
source ./.bashrc
```

Now it should be possible for you to setup ATLAS software. Start by working inside of a singularity container. 
```
setupATLAS -c centos7+batch
```

If setting up ALTAS software did not work for you, try logging into another login node:
```
ssh USER@gra-login>>NUM<<.computecanada.ca
```
Where >>NUM<< has been replaced by a node number such as 1, 2 or 3.

## Create a python virtual environment with and setup an lcg view.
```
git clone https://gitlab.cern.ch/luadamek/lcg_virtualenv.git
git clone https://gitlab.cern.ch/luadamek/hmumuregressionresolution.git
setupATLAS
cd hmumuregressionresolution
git clone https://gitlab.cern.ch/luadamek/massresolutionregression.git
CMTCONFIG=x86_64-centos7-gcc8-opt ./../lcg_virtualenv/create_lcg_virtualenv venv_HmumuRegressionResolution LCG_94python3
source venv_HmumuRegressionResolution/bin/activate
```

## Install a lot of common software packages used by physics analyses @ CERN
```
pip install --upgrade pip
pip install atlas-mpl-style --no-cache-dir
git clone https://github.com/joeycarter/atlas-plots.git
cd atlas-plots
pip install -e .
cd ..
git clone https://github.com/xrootd/xrootd.git
cd xrootd/bindings/python/
git checkout stable-4.11.x
python setup_standalone.py install
cd ../../../
pip install uproot
pip install guppy3
pip install pdfkit
pip install jupyter
```


## NTuple Locations
HZZ nTuples
```
/project/def-psavard/MASSSCRATCHDIR/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/
```

Hmumu nTuples
```
/project/def-psavard/hmumu_harish_files/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/Hmumu/common_ntuples/v23/
```

If you cannot access the above files, you can email lukas.adamek@mail.utoronto.ca, and he should look at this documentation and type the following lines into his terminal:
```
setfacl -d -m u:YOURUSERNAME:rwx /home/ladamek/projects/def-psavard/THEDIRECTORY
```


## Finally, run the setup script
```
source ./setup.sh
```

## Check Installations
You can test that everything has installed correctly with the following lines:
```
root -b #for root
python
import uproot #for uproot
import ROOT #for pyroot
import root_numpy #for root_numpy
import numpy
import tensorflow #a popular machine learning library
import keras # a popular neural network training library
import xgboost #a popular bdt training library
exit()
```

## Login
When you normally login, you don't need to install all of the software again. There is a setup script provided that should restore your environment exactly as it was after installing everything. When working on graham, we don't work on the login node, but rather request a specific computing node with the salloc command.
```
ssh USER@graham.computecanada.ca
salloc --mem=30000M --time=03:00:00
setupATLAS -c centos7+batch
cd >>THE PROJECT DIRECTORY<<
source ./setup.sh
```

## Playing around with Jupyter notebooks
Jupyter notebooks are a great way to play around with data and quickly plot and test things. You can read about them here: https://jupyter.org/ . The following lines of code document how to setup a jupyter notebook.

Open a terminal on your local machine -- not graham -- and do:
```
pip install sshutil
```

Then run the following lines of code and follow the printed insructions.
```
source start_notebook.sh
```

After copying the URL provided into your local machine's browser, you can open the a jupyter session in your browser. You will be prompted in for a token in the browser, which should be printed on the terminal connected to compute canada.

## Generating a grid proxy
You need a grid proxy to access data stored on EOS, or to use rucio to download ATLAS datasets. If you need to do this the following lines of code should set one up. If you need a grid certificate, get one here: https://ca.cern.ch/ca/user/Request.aspx?template=EE2User
```
setupATLAS -c centos7+batch
cd HmumuRegressionResolution
lsetup rucio
export HmumuRegressionResolutionDir=$PWD
export X509_USER_PROXY=${HmumuRegressionResolutionDir}/grid_proxy
voms-proxy-init --voms atlas --hours 10000
exit
```

## Installing RooFit Extensions
```
git clone https://gitlab.cern.ch/atlas_higgs_combination/software/RooFitExtensions.git
cd RooFitExtensions
mkdir build
cd build
cmake .. && make
cd ../../
```
